const supertest = require('supertest');
const { app } = require('../app');

const api = supertest(app);


describe("Testing API route /api/news - Request GET all news", () => {
    test("Trying to get all news without token", async () => {
        await api
            .get("/api/news")
            .set('Content-Type', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, { news: [
                {
                  name: 'Demo',
                  image: 'https://www.designevo.com/res/templates/thumb_small/colorful-hand-and-warm-community.png',
                  createdAt: '2021-07-15T21:02:15.317Z',
                  id: 2
                },
                {
                  name: 'Encuentro de organizaciones',
                  image: 'https://lh3.googleusercontent.com/pw/AM-JKLWVpgPXTFsMe1Ue6cYMzP1iFw14TOBmqdANBahDK76gwkMK5V54L8KjQ8k6AxTFLbIn-9TUJiiq1KJMR37reG51ohHnwenoZ_BhdQ2lWdbp5rzUe4ikupmXIuT9rJWKwmgpuUv9-0WMzQd2I40kgyda6A=w900-h600-no?authuser=0',
                  createdAt: '2022-11-10T01:48:16.393Z',
                  id: 4
                },
                {
                  name: 'Más derechos para los niños',
                  image: 'https://lh3.googleusercontent.com/pw/AM-JKLUTjOfUWt5JncEh9M24xsY82cf3qCkbE0UJOZ0I_RxiVb1cKNDN621K0LviC3lTkhGY9VwqH6jBFXDu0knSIs_zrRcUY_BkU_8dkWXAh5KRj5oN3IXk-tFKexkuO9zsu_B9zOnTiNEsdKqwWCxwNPtcPA=w900-h600-no?authuser=0',
                  createdAt: '2022-11-10T01:48:16.393Z',
                  id: 5
                },
                {
                  name: 'Protección al medioambiente',
                  image: 'https://lh3.googleusercontent.com/pw/AM-JKLVp8NPG6FdvQbxzaZyAV-BU6aVmXhNoA7ElPZdyarRypthEBElXLh4BLY83xlL44P-mLVZ8Ms_b4Z7C_hwV7H0BRYT2fo_dzAS5_X-f2cRuUzFDZYFyWdKeTvyQTG2_vCc8RfZnR9P5foCZ90HkEdOk8A=w900-h600-no?authuser=0',
                  createdAt: '2022-11-10T01:48:16.393Z',
                  id: 6
                }
              ] });
    });

    test("Trying to get all news with token", async () => {
        const userLogged = await api
            .post("/api/auth/login")
            .set("Content-Type", "application/json")
            .send({
                email: "user@mail.com",
                password: "123456"
            });
        await api
            .get("/api/news")
            .set("Content-Type", "application/json")
            .set("Authorization", userLogged.body.token)
            .expect('Content-Type', /json/)
            .expect(200)
    })
})

describe("API route /api/news/:id - Request GET", () => {
    test("Trying to get a new without providing a token", async () => {
        await api
            .get("/api/news/1")
            .set('Content-Type', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, { newsDetail: {
                id: 1,
                name: 'test',
                content: "This is me just editing the new's endpoint",
                image: 'https://www.designevo.com/res/templates/thumb_small/colorful-hand-and-warm-community.png',
                categoryId: 1,
                type: 'Event',
                deletedAt: null,
                createdAt: '2021-07-15T21:02:15.317Z',
                updatedAt: '2022-11-10T01:57:12.188Z'
              }
         });
    })
    test("Tryng to get a new with token", async () => {
        const userLogged = await api
            .post("/api/auth/login")
            .set("Content-Type", "application/json")
            .send({
                email: "user@mail.com",
                password: "123456"
            });
        await api
            .get("/api/news/1")
            .set("Content-Type", "application/json")
            .set("Authorization", userLogged.body.token)
            .expect('Content-Type', /json/)
            .expect(200)
    })
    test("Tryng to get a new with wrong id", async () => {
        const userLogged = await api
            .post("/api/auth/login")
            .set("Content-Type", "application/json")
            .send({
                email: "user@mail.com",
                password: "123456"
            });
        await api
            .get("/api/news/wrong_id")
            .set("Content-Type", "application/json")
            .set("Authorization", userLogged.body.token)
            .expect("Content-Type", /json/)
            .expect(404, { Error: "Entry not found" })
    })
})

describe("API route /api/news/ - Request POST", () => {
    test("Sending request without token", async () => {
        const entryData = {
            name: "test",
            content: "This is me just testing the new's endpoint",
            image: 'https://www.designevo.com/res/templates/thumb_small/colorful-hand-and-warm-community.png',
            categoryId: 1,
            type: "Event"
        }

        await api
            .post("/api/news")
            .set('Content-Type', 'application/json')
            .send(entryData)
            .expect(400, { error: 'No token provided' });



    });

    test("Sending request with no admin token", async () => {
        const userLogged = await api
            .post("/api/auth/login")
            .set("Content-Type", "application/json")
            .send({
                email: "user@mail.com",
                password: "123456"
            });

        const entryData = {
            name: "test",
            content: "This is me just testing the new's endpoint",
            image: 'https://www.designevo.com/res/templates/thumb_small/colorful-hand-and-warm-community.png',
            categoryId: 1,
            type: "Event"
        }

        await api
            .post("/api/news")
            .set('Content-Type', 'application/json')
            .set("Authorization", userLogged.body.token)
            .send(entryData)
            .expect(403, { error: 'Admin role required' });
    })

    test("Sending empty request", async () => {
        const adminLogged = await api
            .post('/api/auth/login')
            .set('Content-Type', 'application/json')
            .send({
                "email": "admin@mail.com",
                "password": "123456"
            });

        await api
            .post("/api/news")
            .set('Content-Type', 'application/json')
            .set('Authorization', adminLogged.body.token)
            .expect(422);
    })
    test("Sending good request with right token", async () => {
        const adminLogged = await api
            .post('/api/auth/login')
            .set('Content-Type', 'application/json')
            .send({
                "email": "admin@mail.com",
                "password": "123456"
            });

        const entryData = {
            name: "test",
            content: "This is me just testing the new's endpoint",
            image: 'https://www.designevo.com/res/templates/thumb_small/colorful-hand-and-warm-community.png',
            categoryId: 1,
            type: "Event"
        }

        const res = await api
            .post("/api/news")
            .set('Content-Type', 'application/json')
            .set('Authorization', adminLogged.body.token)
            .send(entryData)
            .expect(201);

        const entry = res.body;
        console.log(entry);

        expect(entry).toEqual(expect.objectContaining(entryData));

    })

})

describe("API route /api/news/:id - Request PUT", () => {
    test("Sending request without token", async () => {
        const entryData = {
            name: "test",
            content: "This is me just testing the new's endpoint",
            image: 'https://www.designevo.com/res/templates/thumb_small/colorful-hand-and-warm-community.png',
            categoryId: 1,
            type: "Event"
        }

        await api
            .put("/api/news/1")
            .set('Content-Type', 'application/json')
            .send(entryData)
            .expect(400, { error: 'No token provided' });
    })
    test("Sending request with no token admin", async () => {
        const userLogged = await api
            .post("/api/auth/login")
            .set("Content-Type", "application/json")
            .send({
                email: "user@mail.com",
                password: "123456"
            });

        const entryData = {
            name: "test",
            content: "This is me just testing the new's endpoint",
            image: 'https://www.designevo.com/res/templates/thumb_small/colorful-hand-and-warm-community.png',
            categoryId: 1,
            type: "Event"
        }

        await api
            .put("/api/news/1")
            .set('Content-Type', 'application/json')
            .set("Authorization", userLogged.body.token)
            .send(entryData)
            .expect(403, { error: 'Admin role required' });
    })
    test("Sending empty request", async () => {
        const adminLogged = await api
            .post('/api/auth/login')
            .set('Content-Type', 'application/json')
            .send({
                "email": "admin@mail.com",
                "password": "123456"
            });

        await api
            .put("/api/news/1")
            .set('Content-Type', 'application/json')
            .set('Authorization', adminLogged.body.token)
            .expect(422);
    })

    test("Sending request with wrong id", async () => {
        const adminLogged = await api
            .post('/api/auth/login')
            .set('Content-Type', 'application/json')
            .send({
                "email": "admin@mail.com",
                "password": "123456"
            });
        const entryData = {
            name: "test",
            content: "This is me just editing the new's endpoint",
            image: 'https://www.designevo.com/res/templates/thumb_small/colorful-hand-and-warm-community.png',
            categoryId: 1,
            type: "Event"
        }

        await api
            .put("/api/news/wrong_id")
            .set('Content-Type', 'application/json')
            .set('Authorization', adminLogged.body.token)
            .send(entryData)
            .expect(404, { Error: "Entry not found" });

    })
    test("Sending good request with right token", async () => {
        const adminLogged = await api
            .post('/api/auth/login')
            .set('Content-Type', 'application/json')
            .send({
                "email": "admin@mail.com",
                "password": "123456"
            });

        const entryData = {
            name: "test",
            content: "This is me just editing the new's endpoint",
            image: 'https://www.designevo.com/res/templates/thumb_small/colorful-hand-and-warm-community.png',
            categoryId: 1,
            type: "Event"
        }

        const res = await api
            .put("/api/news/1")
            .set('Content-Type', 'application/json')
            .set('Authorization', adminLogged.body.token)
            .send(entryData)
            .expect(200);

        const entry = res.body;

        expect(entry).toEqual(expect.objectContaining(entryData));

    })
})

describe("API route /api/news/:id - Request DELETE", () => {
    test("Sending request without token", async () => {
        await api
            .delete("/api/news/1")
            .set('Content-Type', 'application/json')
            .expect(400, { error: 'No token provided' });
    })
    test("Sending request with no token admin", async () => {
        const userLogged = await api
            .post("/api/auth/login")
            .set("Content-Type", "application/json")
            .send({
                email: "user@mail.com",
                password: "123456"
            });

        await api
            .delete("/api/news/1")
            .set('Content-Type', 'application/json')
            .set("Authorization", userLogged.body.token)
            .expect(403, { error: 'Admin role required' });
    })
    test("Sending request with wrong id", async () => {
        const adminLogged = await api
            .post('/api/auth/login')
            .set('Content-Type', 'application/json')
            .send({
                "email": "admin@mail.com",
                "password": "123456"
            });

        await api
            .delete("/api/news/wrong_id")
            .set('Content-Type', 'application/json')
            .set('Authorization', adminLogged.body.token)
            .expect(404, { Error: "Entry not found" });

    })
    test("Sending request with valid id", async() =>{
        const adminLogged = await api
        .post('/api/auth/login')
        .set('Content-Type', 'application/json')
        .send({
            "email": "admin@mail.com",
            "password": "123456"
        });

        const entry = await api
        .post("/api/news")
        .set("Content-Type", "application/json")
        .set("Authorization", adminLogged.body.token)
        .send({
            name: "test",
            content: "This is the entry's content",
            image: 'https://www.designevo.com/res/templates/thumb_small/colorful-hand-and-warm-community.png',
            categoryId: 1,
            type: "Event"
        })
        .expect(201)

        await api
            .delete(`/api/news/${entry.body.id}`)
            .set("Content-Type", "application/json")
            .set("Authorization", adminLogged.body.token)
            .expect(200, '"Entry successfully deleted"')
    })
})