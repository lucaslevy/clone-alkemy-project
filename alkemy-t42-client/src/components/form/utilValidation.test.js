import {
  validationFirstName,
  validationLastName,
  validationEmail,
  validationPassword,
} from './utilValidation';

describe('/component/form/utilValidation.js - Singles validations are okey', () => {
  test('Is validationFirstName working', () => {
    expect(validationFirstName('Brendam')).toBe(undefined);
    expect(validationFirstName()).toBe('Requerido');
    expect(validationFirstName('B')).toBe('Debe contener 2 caracteres o más.');
    expect(validationFirstName('E'.repeat(19))).toBe(
      'Como máximo 18 caracteres.',
    );
  });
  test('Is validationLastName working', () => {
    expect(validationLastName('Brendam')).toBe(undefined);
    expect(validationLastName()).toBe('Requerido');
    expect(validationLastName('B')).toBe('Debe contener 2 caracteres o más.');
    expect(validationLastName('E'.repeat(19))).toBe(
      'Como máximo 18 caracteres.',
    );
  });
  test('Is validationEmail working', () => {
    expect(validationEmail('Brendam@gmail.com')).toBe(undefined);
    expect(validationEmail()).toBe('Requerido');
    expect(validationEmail('E'.repeat(64) + 'Brendam@gmail.com')).toBe(
      'Debe contener mas de 64 caracteres.',
    );
  });
  test('Is validationPassword working', () => {
    expect(validationPassword('123456')).toBe(undefined);
    expect(validationPassword()).toBe('Requerido');
    expect(validationPassword('12345')).toBe('Debe contener 6 caracteres o más.');
    expect(validationPassword('E'.repeat(33))).toBe(
      'Debe contener un máximo de 32 caracteres.',
    );
  });
});
