import {
  render,
  screen,
  cleanup,
  fireEvent,
  waitFor,
} from '@testing-library/react';
import '@testing-library/jest-dom';
import { MemoryRouter } from 'react-router-dom';
import FormLogin from './FormLogin';

beforeEach(() =>
  render(
    <MemoryRouter>
      <FormLogin />
    </MemoryRouter>,
  ),
);
afterAll(cleanup);

describe('/component/form/login/FormLogin.js - <FormLogin> - Form Login Renders', () => {
  test('Does renders Form Login', () => {
    screen.getByText('Ingrese a la página');
    screen.getByText('Correo electrónico');
    screen.getByText('Contraseña');
    screen.getByText("¿No tiene una cuenta?. Regístrese.");
  });
});

describe('/component/form/login/FormLogin.js - <FormLogin> - Form Login Change Values', () => {
  test('Does change input email', async () => {
    const inputEmail = screen.getByRole('textbox', {
      name: 'Correo electrónico',
    });
    fireEvent.change(inputEmail, {
      target: { value: 'BrendanEich@gmail.com' },
    });
    await waitFor(() => {
      expect(inputEmail.value).toBe('BrendanEich@gmail.com');
    });
  });
  test('Does change input password', async () => {
    const inputPassword = screen.getByLabelText(/Contraseña/i, {
      selector: 'input',
    });
    fireEvent.change(inputPassword, {
      target: { value: '123456' },
    });
    await waitFor(() => {
      expect(inputPassword.value).toBe('123456');
    });
  });
});
