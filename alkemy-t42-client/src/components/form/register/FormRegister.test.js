import {
  render,
  screen,
  cleanup,
  fireEvent,
  waitFor,
} from '@testing-library/react';
import '@testing-library/jest-dom';
import { MemoryRouter } from 'react-router-dom';
import FormRegister from './FormRegister';

beforeEach(() =>
  render(
    <MemoryRouter>
      <FormRegister />
    </MemoryRouter>,
  ),
);
afterAll(cleanup);

describe('/component/form/register/FormRegister.js - <FormRegister> - Form Register Renders', () => {
  test('Does renders Form Register', () => {
    screen.getByText('Crear una cuenta');
    screen.getByText('Nombre');
    screen.getByText('Apellido');
    screen.getByText('Correo electrónico');
    screen.getByText('Contraseña');
    screen.getByText('¿Ya tiene una cuenta?. Ingrese.');
  });
});

describe('/component/form/register/FormRegister.js - <FormRegister> - Form Register Change Values', () => {
  test('Does change input first name', async () => {
    const inputFirstName = screen.getByRole('textbox', { name: 'Nombre' });
    fireEvent.change(inputFirstName, {
      target: { value: 'Brendan' },
    });
    await waitFor(() => {
      expect(inputFirstName.value).toBe('Brendan');
    });
  });
  test('Does change input last name', async () => {
    const inputLastName = screen.getByRole('textbox', {
      name: 'Apellido',
    });
    fireEvent.change(inputLastName, {
      target: { value: 'Eich' },
    });
    await waitFor(() => {
      expect(inputLastName.value).toBe('Eich');
    });
  });
  test('Does change input email', async () => {
    const inputEmail = screen.getByRole('textbox', {
      name: 'Correo electrónico',
    });
    fireEvent.change(inputEmail, {
      target: { value: 'BrendanEich@gmail.com' },
    });
    await waitFor(() => {
      expect(inputEmail.value).toBe('BrendanEich@gmail.com');
    });
  });
  test('Does change input password', async () => {
    const inputPassword = screen.getByLabelText(/Contraseña/i, {
      selector: 'input',
    });
    fireEvent.change(inputPassword, {
      target: { value: '123456' },
    });
    await waitFor(() => {
      expect(inputPassword.value).toBe('123456');
    });
  });
});
